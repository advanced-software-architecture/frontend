# Stage 1: Build React app
FROM node:18.17.1 AS build

# Set working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Build React app
RUN npm run build

# Stage 2: Serve React app on port 80 without Nginx
FROM node:18.17.1

# Set working directory
WORKDIR /app

# Copy the built React app from the build stage
COPY --from=build /app/build /app

# Expose port 80
EXPOSE 80

# Install serve to run the app
RUN npm install -g serve

# Command to start serving the React app on port 80
CMD ["serve", "-p", "80", "-s", "."]
